<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Redbox\DoubleClick\Observer;

use Magento\Framework\Event\ObserverInterface;

class OnOrderSuccessPageViewObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\ViewInterface
     */
    protected $view;

    /**
     * @param \Magento\Framework\App\ViewInterface $view
     */
    public function __construct(
        \Magento\Framework\App\ViewInterface $view
    ) {
        $this->view = $view;
    }

    /**
     * Add order information into DoubleClick block to render on checkout success pages
     * Fired by the checkout_onepage_controller_success_action and
     * checkout_multishipping_controller_success_action events
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $orderIds = $observer->getEvent()->getOrderIds();
        if (empty($orderIds) || !is_array($orderIds)) {
            return $this;
        }
        /** @var \Redbox\DoubleClick\Block\DoubleClick $block */
        $block = $this->view->getLayout()->getBlock('dc_order_success');
        if ($block) {
            $block->setOrderIds($orderIds);
        }

        return $this;
    }
}
