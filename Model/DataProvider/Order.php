<?php

namespace Redbox\DoubleClick\Model\DataProvider;

class Order
{
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $productFactory;

    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $salesOrderCollection;

    /**
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $salesOrderCollection
     */
    public function __construct(\Magento\Catalog\Model\ProductFactory $productFactory, \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository, \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $salesOrderCollection)
    {
        $this->productFactory = $productFactory;
        $this->categoryRepository = $categoryRepository;
        $this->salesOrderCollection = $salesOrderCollection;
    }

    /**
     * @param array $orderIds
     * @return array
     */
    public function getData(array $orderIds)
    {
        $collection = $this->salesOrderCollection->create();
        $collection->addFieldToFilter('entity_id', ['in' => $orderIds]);

        $products = [];
        $result = [];
        /** @var \Magento\Sales\Model\Order $order */
        foreach ($collection as $order) {
            $revenue = 0;
            if ($order->getBaseGrandTotal() > 0) {
                $revenue = $order->getBaseGrandTotal() -
                    ($order->getBaseTaxAmount() + $order->getBaseShippingAmount());
            }

            $result['id'] = $order->getId();
            $result['revenue'] = $revenue;

            $ind = 0;
            /** @var \Magento\Sales\Model\Order\Item $item */
            foreach ($order->getAllVisibleItems() as $item) {

                $productCategory = '';
                $product = $this->getProductById($item->getProductId());
                $ids = $product->getCategoryIds();
                if ($category = $this->categoryRepository->get(end($ids))) {
                    $productCategory = $category->getName();
                }
                $products[$ind]['id'] = $item->getSku();
                $products[$ind]['name'] = $item->getName();
                $products[$ind]['category'] = $productCategory;
                $products[$ind]['quantity'] = round($item->getQtyOrdered(), 1);

                $ind ++;
            }
            $result['products'] = $products;
        }

        return $result;
    }

    /**
     * @param int $productId
     * @return \Magento\Catalog\Model\Product
     */
    protected function getProductById($productId)
    {
        $product = $this->productFactory->create();
        $productResource = $product->getResource();
        $productResource->load($product, $productId);

        return $product;
    }
}
