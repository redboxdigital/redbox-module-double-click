<?php

namespace Redbox\DoubleClick\Block;

/**
 * Class DoubleClick
 * used for login, machine and registration checkout step events tracking
 * @package Redbox\DoubleClick\Block
 */
class DoubleClick extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Redbox\DoubleClick\Model\DataProvider\Order
     */
    private $orderProvider;

    /**
     * @var \Magento\Framework\Session\SessionManager
     */
    protected $sessionManager;


    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
                                \Redbox\DoubleClick\Model\DataProvider\Order $orderProvider,
                                \Magento\Framework\Session\SessionManager $sessionManager,
                                array $data = [])
    {
        $this->orderProvider = $orderProvider;
        $this->sessionManager = $sessionManager;

        parent::__construct($context, $data);
    }

    public function getOrdersData()
    {
        $orderIds = $this->getOrderIds();
        if (!isset($orderIds) && empty($orderIds) || !is_array($orderIds)) {
            return [];
        }

        return $this->orderProvider->getData($orderIds);
    }

    public function getSessionId()
    {
        return (int) filter_var(
            $this->sessionManager->getSessionId(),
            FILTER_SANITIZE_NUMBER_INT
        );
    }
}
