/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Redbox_DoubleClick/js/dc-checkout-helper',
    'Magento_Checkout/js/view/payment'
], function ($, CheckoutHelper, payment) {
    'use strict';

    function notify(stepName) {

        var Helper = new CheckoutHelper(stepName);

        $(document.body).prepend(Helper.getDoubleClickScript());

    }

    return function (config) {
        var subscription = payment.prototype.isVisible.subscribe(function (value) {
            if (value) {
                notify('payment');
                notify('billing');
                subscription.dispose();
            }
        });

        var stepName = config.checkoutStepName ? config.checkoutStepName : 'shipping';
        notify(stepName);
    };
});
