/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'underscore'
], function ($, _) {
    'use strict';

    /**
     * Checkout helper class
     *
     * @param {string} stepName
     */
    function CheckoutHelper(stepName) {
            this.stepName = stepName;
    }

    CheckoutHelper.prototype = {
        /**
         * @return string
         *
         * @public
         */
        getDoubleClickScript: function() {
            var content = _.template(this.template);
            var currentParam = this.map[this.stepName];
            if (currentParam) {
                var tmplParam = {
                    type: currentParam.type,
                    category:currentParam.category,
                    name:currentParam.name,
                    random: this._getRandomNumber()
                };

                return  content(tmplParam);
            }

            return  '';
        },

        /**
         * @return number
         *
         * @private
         */
        _getRandomNumber: function() {

            return Math.random() * 10000000000000;
        },

        map:{
            shipping: {
                name: 'Shipping_ZA_EN',
                type: 'shipp797',
                category: 'Shipp007'
            },
            payment: {
                name: 'Payment_ZA_EN',
                type: 'payme048',
                category: 'Payme008'
            },
            billing: {
                name: 'Billing_ZA_EN',
                type: 'billi713',
                category: 'Billi008'
            }
        },

        template: "<!--\n" +
            "Start of DoubleClick Floodlight Tag: Please do not remove\n" +
            "Activity name of this tag: <%= name %>\n" +
            "URL of the webpage where the tag is expected to be placed: https://buynespresso.com/za_en/checkout/onepage/\n" +
            "This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.\n" +
            "Creation Date: 12/13/2017\n" +
            "-->\n" +
            "<iframe src=\"https://4137862.fls.doubleclick.net/activityi;src=4137862;type=<%= type %>;cat=<%= category %>;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=<%= random %>?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>\n" +
            "</script>\n" +
            "<noscript>\n" +
            "<iframe src=\"https://4137862.fls.doubleclick.net/activityi;src=4137862;type=<%= type %>;cat=<%= category %>;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>\n" +
            "</noscript>\n" +
            "<!-- End of DoubleClick Floodlight Tag: Please do not remove -->"

    };

    return CheckoutHelper;
});
